<?php

use Doctrine\Common\Util\Debug;

function w($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}

function wd($data){
    echo '<pre>';
    print_r(var_dump($data));
    echo '</pre>';
    die();
}

function d($data, $maxDepth = 2){
    echo '<pre>';
    Debug::dump($data, $maxDepth);
    echo '</pre>';
    die();
}