<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\Routing\Annotation\Route,  
    Symfony\Component\HttpFoundation\Response;

use Doctrine\Bundle\DoctrineBundle\Registry;

use App\Entity\User;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     * @return Response
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'welcome' => 'Bienvenido al curso de sf5',
            'prueba'  => '(Pido perdón, es mi primer pgm.)',
        ]);
    }

    /**
     * @Route("/users", name="user_list")
     * @return Response
     */
    public function userListAction()
    {
        /** @var $doctrine Registry */
        $doctrine = $this->get('doctrine');

        /** @var $items */
        $items = $doctrine
            ->getManager('default')
            ->getRepository(User::class)
            ->findAll();

        return $this->render('default/user_list.html.twig', [
            'items' => $items
        ]);
    }
    
    /**
     * @Route("/user/{id}", name="user_detail")
     * @return Response
     */
    public function userDetailAction($id)
    {
        /** @var $doctrine Registry */
        $doctrine = $this->get('doctrine');

        /** @var $item User */
        $item = $doctrine->getManager('default')
            ->getRepository(User::class)
            ->find($id);
        
        return $this->render('default/user_detail.html.twig', [
            'item' => $item
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="user_delete")
     * @return Response
     */
    public function userDeleteAction(User $item)
    {
        $return = $this->get('router')
                ->generate('user_list');
        
        /** @var $entityManager */
        $entityManager = $this->get('doctrine')
                ->getManager('default');
        
        // Eliminar
        $entityManager->remove($item);
        $entityManager->flush();
        
        // Existe redirect user_list con mensaje de exito
        return new RedirectResponse($return);
    }
    
    /**
     * @Route("/view/json/{id}", name="user_to_json")
     * @return Response
     */
    public function userToJson($id)
    {
        return $this->json(['nombre' => 'sdsds']);
    }
}
